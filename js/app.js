var menuContainer = document.getElementsByClassName('navigation')[0];
var menuIcon = document.getElementsByClassName('hamburger-icon')[0];

menuIcon.addEventListener('click', function(event){
  event.preventDefault();
  this.classList.toggle('active');
  menuContainer.classList.toggle('hamburger-active');
});

